# gitRannex

Manage big files from within R via git-annex.

## Installation

Install using the `devtools` package:

    library("devtools")
    install_git("git@gitlab.com:gitrannex/gitrannex.git")