#' gitRannex
#' 
#' @name gitRannex
#' @docType package
NULL

##' start a shell in the current wd in which git-annex and all other
##' needed tools are available and use that shell to execute the given
##' command
##'
##' @title run a given shell command in the git-annex shell
##' @param command character.  optional.  shell command to execute.  If NULL
##' starts an interactive shell session.  defaults to NULL.
##' @param wd character string. optional.  change into that directory
##' prior to command execution.  defaults to getwd().
##' @param verbose logical. optional.  if TRUE print some messages.
##' defaults to TRUE
##' @param nocheck logical. optional.  if TRUE do not perform safety checks.
##' defaults to FALSE.
##' @param ... additional args will be passed to system()
##' @return integer.  the error code of running command.  0 in case of no error.
##' @author Andreas Leha
##' @export
gra_runInShell <- function(command=NULL,
                           wd=getwd(),
                           verbose=TRUE,
                           nocheck=FALSE,
                           ...) {
  oldwd <- getwd()
  setwd(wd)
  on.exit(setwd(oldwd))

  if (verbose) {
    message(paste("gitRannex:", "operating in", wd))
    if (is.null(command)) {
      message(paste("gitRannex:", "leave this shell via the command 'exit'"))
    }
  }
      
  if (!nocheck) {
    ## check if 'runshell' from git-annex is present
    if (!file.exists(system.file(file.path("git-annex", "runshell"),
                                 package = "gitRannex"))) {
      stop("can not find the 'runshell' script from git-annex at the expected location, please check your installation of 'gitRannex'")
    }
    ## check if 'runshell' from git-annex is execute
    if (!file.access(system.file(file.path("git-annex", "runshell"),
                                 package = "gitRannex"),
                     mode=1) == 0) {
      stop("the 'runshell' script from git-annex is not executable, please check your installation of 'gitRannex'")
    }
  }
  
  ## execute command
  system(paste(system.file(file.path("git-annex", "runshell"),
                           package = "gitRannex"),
               command),
         ...)
}


##' run 'git annex init'
##'
##' @title wrapper around 'git annex init'
##' @param wd character string. optional.  change into that directory
##' prior to command execution.  defaults to getwd().
##' @return integer. status of system call.  should be 0 if no error
##' @author Andreas Leha
##' @export
gra_init <- function(wd=getwd()) {
  gra_runInShell("git annex init", wd=wd)
}


##' run 'git annex status'
##'
##' @title  wrapper around 'git annex status'
##' @param wd character string. optional.  change into that directory
##' prior to command execution.  defaults to getwd().
##' @return integer. status of system call.  should be 0 if no error
##' @author Andreas Leha
##' @export
gra_status <- function(wd=getwd()) {
  gra_runInShell("git annex status", wd=wd)
}


##' run 'git annex init'
##'
##' @title  wrapper around 'git annex add'
##' @param file character string.  which file to add.
##' @param wd character string. optional.  change into that directory
##' prior to command execution.  defaults to getwd().
##' @return integer. status of system call.  should be 0 if no error
##' @author Andreas Leha
##' @export
gra_add <- function(file, wd=getwd()) {
  gra_runInShell(paste("git annex add", file), wd=wd)
}


##' run 'git annex get'
##'
##' @title  wrapper around 'git annex get'
##' @param file character string.  which file to get.
##' @param wd character string. optional.  change into that directory
##' prior to command execution.  defaults to getwd().
##' @return integer. status of system call.  should be 0 if no error
##' @author Andreas Leha
##' @export
gra_get <- function(file, wd=getwd()) {
  gra_runInShell(paste("git annex get", file), wd=wd)
}


##' run 'git annex drop'
##'
##' @title  wrapper around 'git annex drop'
##' @param file character string.  which file to drop.
##' @param wd character string. optional.  change into that directory
##' prior to command execution.  defaults to getwd().
##' @return integer. status of system call.  should be 0 if no error
##' @author Andreas Leha
##' @export
gra_drop <- function(file, wd=getwd()) {
  gra_runInShell(paste("git annex drop", file), wd=wd)
}


##' present annexed files and their repository in a data.frame
##'
##' @title wrapper around 'git annex whereis'
##' @param wd character string. optional.  change into that directory
##' prior to command execution.  defaults to getwd().
##' @return data.frame.  first column will contain the annexed files.
##' the later columns represent the known repositories and contain '*' if
##' the file is present in that repository
##' @author Andreas Leha
##' @export
gra_ls <- function(wd=getwd()) {
  ga_output <- suppressWarnings(gra_runInShell("git annex whereis",
                                               wd=wd,
                                               intern=TRUE))

  if (!is.null(attributes(ga_output)) && attributes(ga_output)$status != 0)
      stop("gitRannex: error running 'git annex whereis'")

  if (length(ga_output)==0) {
    message("gitRannex: no files annexed")
    return(data.frame())
  }
  
  idx_start <- grep("^whereis", ga_output)
  idx_end <- c(idx_start[-1], length(ga_output) + 1) - 1

  ga_files <- gsub("^whereis ", "", ga_output[idx_start])
  ga_files <- gsub(" \\([[:digit:]]* copy\\)[[:space:]]*$", "", ga_files)

  res_df <- data.frame(file=ga_files,
                       idx_start=idx_start,
                       idx_end=idx_end)

  repos <- 
      lapply(1:nrow(res_df),
             function(r)
           {
             x <- as.character(res_df[r,])
             sapply((as.numeric(x[2])+1):(as.numeric(x[3])-1), function(i) {
               repo <- gsub(".* -- ", "", ga_output[i])
               repo <- gsub(" \\(.*\\)$", "", repo)
               repo
             })
           })

  allrepos <- unlist(repos)

  repos_df <- 
      do.call(rbind,
              lapply(repos, function(lrepos)
                   {
                     lpresent <- ifelse(allrepos %in% lrepos, "*", "")
                     lpresent <- as.data.frame(t(lpresent))
                     colnames(lpresent) <- allrepos
                     lpresent
                   }))

  present_here <- repos_df$here
  if (is.null(present_here))
      present_here <- rep("", nrow(res_df))
  
  res_df <- data.frame(file=res_df[,1],
                       here=present_here,
                       repos_df[,colnames(repos_df) != "here", drop=FALSE])

  colnames(res_df)[1] <- ""

  res_df
}
